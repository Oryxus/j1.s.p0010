package Controller;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Management {

    Scanner in;
    Helpers helpers;

    public Management() {
        in = new Scanner(System.in);
        helpers = new Helpers();
    }
    
    protected void menu() {
        int len = 0;
        int x = 0;
        int[] arr;
        int position = -1;
        while (true) {
            System.out.println("Enter number of array: ");
            len = helpers.checkInt();
            
            System.out.println("Enter number wanna find: ");
              x = helpers.checkInt();
           
              arr = randomArray(len);
            System.out.println(Arrays.toString(arr));
             
            position = linearSearch(x, arr);
            if (position != -1) {
                System.out.println("the index of number: " + position);
            } else {
                System.out.println("Can not find number in array");
            }
        }
    }

    protected int[] randomArray(int len) {
        Random random = new Random();
        int[] arr = new int[len];
        for (int i = 0; i < len; i++) {
            arr[i] = random.nextInt(len);
        }
        return arr;
    }

    protected int linearSearch(int x, int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            if (x == arr[i]) {
                return i;
            }
        }
        return -1;
    }
    
}
